[![Build Status](https://img.shields.io/gitlab/pipeline/rkaippully/juxta/master)](https://gitlab.com/e/rkaippully/juxta/pipelines)
[![Hackage](https://img.shields.io/hackage/v/gamgee)](https://hackage.haskell.org/package/juxta)

# About juxta
Juxta helps to compare the behavior of different versions of API services.
