----------------------------------------------------------------------------------------------------
-- Generate an hpack package.yaml file from dhall
----------------------------------------------------------------------------------------------------

let gitlab-url = "https://gitlab.com/rkaippully/juxta"

let package-info =
    { name               = "juxta"
    , version            = "0.1.0"
    , synopsis           = ""
    , description        = "Please see the README at <" ++ gitlab-url ++ "#readme>"
    , category           = ""
    , homepage           = gitlab-url ++ "#readme"
    , bug-reports        = gitlab-url ++ "/issues"
    , author             = "Raghu Kaippully"
    , maintainer         = "rkaippully@gmail.com"
    , copyright          = "2019 Raghu Kaippully"
    , license            = "MPL-2.0"
    , extra-source-files = [ "ChangeLog.md"
                           , "README.md"
                           ]
    , git                = gitlab-url
    }

let common-fields =
    { ghc-options       = [ "-Wall"
                          , "-Wcompat"
                          , "-Wredundant-constraints"
                          , "-Wincomplete-record-updates"
                          , "-Wincomplete-uni-patterns"
                          ]
    , default-extensions = ./default-extensions.dhall
    , dependencies       = [ "base >=4.12.0.0 && <4.13"
                           , "relude ==0.5.0"
                           ]
    }

let library =
    { library =
      { source-dirs     = "src"
      , dependencies    = [ "base >=4.12.0.0 && <4.13"
                          , "relude ==0.5.0"
                          , "http-client ==0.6.4"
                          , "http-client-tls ==0.3.5.3"
                          , "http-types ==0.12.3"
                          , "wai ==3.2.2.1"
                          , "warp ==3.2.28"
                          , "stm ==2.5.0.0"
                          , "safe-exceptions ==0.1.7.0"
                          , "async ==2.2.2"
                          ]
      }
    }

let executable =
    { executable =
      { main         = "Main.hs"
      , source-dirs  = "app"
      , dependencies = [ "juxta" ]
      , ghc-options  = [ "-threaded"
                       , "-rtsopts"
                       , "-with-rtsopts=-N"
                       ]
      }
    }

let tests =
    { tests =
      { juxta-test =
        { main         = "Main.hs"
        , source-dirs  = "test"
        , dependencies = [ "juxta" ]
        , ghc-options  = [ "-threaded"
                         , "-rtsopts"
                         , "-with-rtsopts=-N"
                         ]
        }
      }
    }

in package-info // common-fields // library // executable // tests
