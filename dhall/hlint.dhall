let Prelude/List/map = https://raw.githubusercontent.com/dhall-lang/dhall-lang/v8.0.0/Prelude/List/map

let Rule = ./hlint/relude/Rule.dhall

in

[ Rule.Arguments
  { arguments =
      Prelude/List/map Text Text
        (\(extnName : Text) -> "-X${extnName}")
        ./default-extensions.dhall
  }
] # ./hlint/relude.dhall
