-- | Compare two responses
module Juxta.Comparator
  ( Handle
  , initComparator
  , compareResponses
  ) where

import qualified Control.Concurrent.Async as Async
import qualified Juxta.Config             as Cfg
import qualified Juxta.Diffs              as Diff
import           Relude                   hiding (Handle)


data Handle = Handle
  { otherService :: Cfg.ServiceConfig
  , diffs        :: TVar Diff.DiffStore
  }

initComparator :: MonadIO m
               => Cfg.Config
               -> m Handle
initComparator cfg = liftIO $ Handle (Cfg.otherService cfg) <$> newTVarIO Diff.emptyStore

compareResponses :: MonadIO m
                 => Handle
                 -> Cfg.ServiceResponse
                 -> Async.Async Cfg.ServiceResponse
                 -> m ()
compareResponses handle baseResp otherThread = liftIO $ void $ Async.async $ do
  otherResp <- Async.wait otherThread
  case (baseResp, otherResp) of
    (Cfg.ServiceResponse resp1, Cfg.ServiceResponse resp2) ->
      when (Cfg.responseComparator (otherService handle) resp1 resp2) $
        addDiff otherResp
    _ -> addDiff otherResp
  where
    addDiff otherResp = do
      let diff = Diff.Diff baseResp otherResp
      atomically $ modifyTVar' (diffs handle) $ Diff.addDiff diff
