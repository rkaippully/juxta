-- | Configuration for Juxta
module Juxta.Config
  ( Config (..)
  , ServiceName (..)
  , ServiceConfig (..)
  , ServiceType (..)
  , ServiceResponse (..)
  , Response
  ) where

import qualified Network.HTTP.Client      as HTTPClient
import qualified Network.Wai.Handler.Warp as Warp
import           Relude


type Response = HTTPClient.Response LByteString

data Config = Config
  { -- | The service considered source of truth
    baseService     :: ServiceConfig
    -- | The other service to compare
  , otherService    :: ServiceConfig
    -- ^ Server settings for processing requests
  , serverSettings  :: Warp.Settings
    -- ^ Monitor server settings
  , monitorSettings :: Warp.Settings
  }

newtype ServiceName = ServiceName Text
  deriving newtype (Eq, Ord, Hashable)

data ServiceType = BaseService | OtherService

data ServiceConfig = ServiceConfig
  { -- | Name of the service, must be a unique value
    serviceName         :: ServiceName
    -- | Type of the service
  , serviceType         :: ServiceType
    -- | HTTP client connection settings
  , httpSettings        :: HTTPClient.ManagerSettings
    -- | A function to modify the incoming request before it is passed to a service
  , requestInterceptor  :: HTTPClient.Request -> HTTPClient.Request
    -- | A function to modify the response before it is compared
  , responseInterceptor :: Response -> Response
    -- ^ A function to compare two responses, Returns True iff the responses are considered conforming
  , responseComparator  :: Response -> Response -> Bool
  }

data ServiceResponse = ServiceResponse Response
                     | ServiceFailed HTTPClient.HttpException
