-- | Data structures to handle diffs
module Juxta.Diffs
  ( DiffStore
  , Diff(..)
  , emptyStore
  , addDiff
  , getDiff
  , sizeOfStore
  ) where

import qualified Juxta.Config     as Cfg
import           Relude
import qualified Relude.Extra.Map as Map


data Diff = Diff
  { diffBaseResponse  :: Cfg.ServiceResponse
  , diffOtherResponse :: Cfg.ServiceResponse
  }

data DiffStore = DiffStore !Int !(IntMap Diff)

emptyStore :: DiffStore
emptyStore = DiffStore 0 mempty

addDiff :: Diff -> DiffStore -> DiffStore
addDiff diff (DiffStore n diffs) = DiffStore (n + 1) $ Map.insert n diff diffs

sizeOfStore :: DiffStore -> Int
sizeOfStore (DiffStore n _) = n

getDiff :: Int -> DiffStore -> Maybe Diff
getDiff k (DiffStore _ diffs) = Map.lookup k diffs
