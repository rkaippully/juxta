-- | Handles interactions with the services
module Juxta.Service
  ( Handle
  , initService
  , sendToBaseService
  , sendToOtherService
  ) where

import qualified Control.Concurrent.Async as Async
import           Control.Exception.Safe   (catch)
import qualified Juxta.Config             as Cfg
import qualified Network.HTTP.Client      as HTTPClient
import           Relude                   hiding (Handle)


data Handle = Handle
  { baseManager  :: HTTPClient.Manager
  , otherManager :: HTTPClient.Manager
  , baseService  :: Cfg.ServiceConfig
  , otherService :: Cfg.ServiceConfig
  }

initService :: MonadIO m
            => Cfg.Config
            -> m Handle
initService cfg = liftIO $ Handle
  <$> HTTPClient.newManager (Cfg.httpSettings (Cfg.baseService cfg))
  <*> HTTPClient.newManager (Cfg.httpSettings (Cfg.otherService cfg))
  <*> pure (Cfg.baseService cfg)
  <*> pure (Cfg.otherService cfg)

sendToBaseService :: MonadIO m
                  => Handle
                  -> HTTPClient.Request
                  -> m (Async.Async Cfg.ServiceResponse)
sendToBaseService handle = sendToService (baseService handle) (baseManager handle)

sendToOtherService :: MonadIO m
                   => Handle
                   -> HTTPClient.Request
                   -> m (Async.Async Cfg.ServiceResponse)
sendToOtherService handle = sendToService (otherService handle) (otherManager handle)

sendToService :: MonadIO m
              => Cfg.ServiceConfig
              -> HTTPClient.Manager
              -> HTTPClient.Request
              -> m (Async.Async Cfg.ServiceResponse)
sendToService cfg mgr req = liftIO $ Async.async $ sendToService' `catch` handleFail
  where
    sendToService' = Cfg.ServiceResponse . Cfg.responseInterceptor cfg <$> HTTPClient.httpLbs req' mgr

    req' = Cfg.requestInterceptor cfg req

    handleFail = pure . Cfg.ServiceFailed
