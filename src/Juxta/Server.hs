-- | The Juxta proxy server
module Juxta.Server
  ( runServer
  ) where

import qualified Control.Concurrent.Async as Async
import           Control.Exception.Safe   (finally)
import qualified Juxta.Comparator         as Comp
import qualified Juxta.Config             as Cfg
import qualified Juxta.Service            as Service
import qualified Network.HTTP.Client      as HTTPClient
import qualified Network.HTTP.Types       as HTTPTypes
import qualified Network.Wai              as Wai
import qualified Network.Wai.Handler.Warp as Warp
import           Relude


runServer :: MonadIO m => Cfg.Config -> m ()
runServer cfg = do
  -- Get handles
  service <- Service.initService cfg
  comp <- Comp.initComparator cfg

  -- Start the monitor server
  monitor <- startMonitor cfg comp

  -- Start the server
  liftIO $ Warp.runSettings
             (Cfg.serverSettings cfg)
             (serverApp service comp)
           `finally`
             Async.cancel monitor

serverApp :: Service.Handle
          -> Comp.Handle
          -> Wai.Application
serverApp service comp waiRequest respond = do
  let req = toHTTPClientRequest waiRequest

  -- Send request to both services asynchronously
  baseThread <- Service.sendToBaseService service req
  otherThread <- Service.sendToOtherService service req

  baseResp <- Async.wait baseThread

  -- Perform response comparison asynchronously
  Comp.compareResponses comp baseResp otherThread

  -- Send the response from the base service to our caller
  respond $ toWaiResponse baseResp


toHTTPClientRequest :: Wai.Request -> HTTPClient.Request
toHTTPClientRequest wr = HTTPClient.defaultRequest
  { HTTPClient.method = Wai.requestMethod wr
  , HTTPClient.secure = Wai.isSecure wr
  , HTTPClient.path = Wai.rawPathInfo wr
  , HTTPClient.queryString = Wai.rawQueryString wr
  , HTTPClient.requestHeaders = Wai.requestHeaders wr
  , HTTPClient.requestBody = HTTPClient.RequestBodyStreamChunked gp
  , HTTPClient.requestVersion = Wai.httpVersion wr
  }
  where
    gp :: HTTPClient.GivesPopper ()
    gp np = np $ Wai.getRequestBodyChunk wr

toWaiResponse :: Cfg.ServiceResponse -> Wai.Response
toWaiResponse (Cfg.ServiceResponse res) = Wai.responseLBS
  (HTTPClient.responseStatus res) (HTTPClient.responseHeaders res) (HTTPClient.responseBody res)
toWaiResponse (Cfg.ServiceFailed ex) = Wai.responseLBS
  HTTPTypes.badGateway502 [] (encodeUtf8 $ displayException ex)

startMonitor :: MonadIO m
             => Cfg.Config
             -> Comp.Handle
             -> m (Async.Async ())
startMonitor cfg comp = liftIO $ Async.async $
  Warp.runSettings (Cfg.monitorSettings cfg) (monitorApp comp)

monitorApp :: Comp.Handle -> Wai.Application
monitorApp = undefined
